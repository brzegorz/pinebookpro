sudo pacman -S bamf vala
git clone https://gitlab.com/vala-panel-project/vala-panel-appmenu.git
cd vala-panel-appmenu/
git submodule init && git submodule update --remote --merge
mkdir build && cd build
cmake -DENABLE_XFCE=ON -DCMAKE_INSTALL_PREFIX=/usr ..
make -j6
sudo make install
cmake .. -DDISABLE_WERROR=1 -DSDL_BACKEND=SDL2 -DOPENGL_BACKEND=OPENGL -DSTATIC_LINK=enabled
cd $HOME
sudo pacman -S appmenu-qt4 libdbusmenu-glib libdbusmenu-gtk3 libdbusmenu-gtk2
git clone https://aur.archlinux.org/appmenu-gtk-module-git.git
ls
cd appmenu-gtk-module-git/
nano PKGBUILD 
echo "Add aarch64 to PKGBUILD arch list"
makepkg -si
echo "Transaction failed due to existing files. The plugin works anyway"
rm -rf appmenu-gtk-module-git
xfconf-query -c xsettings -p /Gtk/Modules -n -t string -s "appmenu-gtk-module"
xfconf-query -c xsettings -p /Gtk/ShellShowsMenubar -n -t bool -s true
xfconf-query -c xsettings -p /Gtk/ShellShowsAppmenu -n -t bool -s true
reboot
