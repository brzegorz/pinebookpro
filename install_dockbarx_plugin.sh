git clone https://aur.archlinux.org/pikaur.git
# I like pikaur. Feel free to use git clone + makepkg -si for the AUR packages
cd pikaur/
makepkg -si
cd $HOME
rm -rf pikaur/
echo "Add aarch64 to each of the PKGBUILD files"
sudo pacman -S python-pip
sudo pip install polib
sudo pikaur -S xfce4-dockbarx-plugin-gtk3-git 
# That's it
