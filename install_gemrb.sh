cd Downloads
tar -xvf gemrb-0.8.6.tar.gz 
mkdir Builds/gemrb
mv Downloads/gemrb-0.8.6 Builds/gemrb
cd ~/Builds/gemrb/gemrb-0.8.6
mkdir build
cd build
cmake .. -DDISABLE_WERROR=1 -DSDL_BACKEND=SDL2 -DOPENGL_BACKEND=OPENGL -DSTATIC_LINK=enabled
make -j6
sudo make install
cp ../gemrb/GemRB.cfg.sample.in <DESIRED_PATH>/gemrb.cfg
# Now modify your gemrb.cfg. The engine is located at /usr/local
